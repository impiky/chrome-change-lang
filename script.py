import sandboxie
import pyodbc
import configparser
import os
import time
import pickle
import json
import random

SANDBOX_SETTINGS_PATH = r'C:\Windows\Sandboxie.ini'
VMCONFIG_FILE_PATH = r'C:\\abc\\abcmobi.ini'
SANDBOXIES = "C:\\Sandbox\\alks\\"


def getConfig(section, key, file):
    config = configparser.ConfigParser()
    config.read(file, encoding="utf-8")
    return config.get(section, key)
    

Driver = getConfig("database", "Driver", "config.ini")
SERVER = getConfig("database", "SERVER", "config.ini")
DATABASE = getConfig("database", "DATABASE", "config.ini")
UID = getConfig("database", "UID", "config.ini")
PWD = getConfig("database", "PWD", "config.ini")
chromepath = getConfig("chrome", "chromedriverpath", "config.ini")
VMID = getConfig("VMInfo", "VMID", VMCONFIG_FILE_PATH)


class DbManager:
    def __init__(self):
        self.driver = Driver
        self.server = SERVER
        self.database = DATABASE
        self.uid = UID
        self.pwd = PWD
        self.conn = None
        self.cur = None

    # 连接数据库
    # connect to database
    def connectDatabase(self):
        co = "DRIVER={};SERVER={};DATABASE={};UID={};PWD={}".format('{' + self.driver + '}', self.server,
                                                                    self.database, self.uid, self.pwd)
        try:
            self.conn = pyodbc.connect(co)
            self.cur = self.conn.cursor()
            return True
        except:
            print("Error:Can't connect to database")
            return False

    # 关闭数据库
    # close db
    def close(self):
        # 如果数据打开，则关闭；否则没有操作
        # close connection to db
        if self.conn and self.cur:
            self.cur.close()
            self.conn.close()
        return True

    def execute(self, sql, params=None, commit=False, ):
        # 连接数据库
        # connect to db
        res = self.connectDatabase()
        if not res:
            return False
        try:
            if self.conn and self.cur:
                # 正常逻辑，执行sql，提交操作
                # if all good execute sql query
                rowcount = self.cur.execute(sql)
                self.conn.commit()
                
        except:
            print("execute failed: " + sql)
            print("params: " + str(params))
            self.close()
            return False
        return rowcount

    def fetchone(self, sql, params=None):
        res = self.execute(sql, params)
        if not res:
            print("查询失败")
            return False
        result = self.cur.fetchone()
        self.close()
        return result

    def fetchall(self, sql, params=None):
        res = self.execute(sql)
        if not res:
            print("execute failed")
            return False
        results = self.cur.fetchall()
        # print("жџҐиЇўж€ђеЉџ" + str(results))
        self.close()
        return results


def update_browser_lang_field(data, buyer_id):
    db_manager = DbManager()
    db_manager.execute("UPDATE Buyer SET BrowserLanguage='{}' WHERE BuyerID='{}'".format(data, buyer_id))


def run_sandboxed_chrome(sbie, sandboxies_names):
    choises = ["ru", "en", "be", "kk", "lv", "lt" ,"de,de-AT" ,"de-DE", "de-LI" , "de-CH", "pl", "ro" ,"uk"]
    for i in range(0, len(sandboxies_names) + 1):
        rand_choises = tuple()
        sb_chrome_path = 'C:\Sandbox\\alks\\{}\\user\\current\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\Preferences'.format(sandboxies_names[i])
        try:
            sbie.start('C:\Program Files (x86)\Google\Chrome\Application\chrome.exe', box=sandboxies_names[i], wait=False)
            time.sleep(20)
            sbie.terminate_processes(box=sandboxies_names[i])
            with open(sb_chrome_path, "r+") as f:
                data = json.load(f)
                if random.randint(0, 100) > 50:
                    rand_choises = (choises[random.randint(0,3)], choises[random.randint(4,8)], choises[random.randint(9,12)])
                    data["intl"]["accept_languages"] = "{}, {}, {}".format(rand_choises[0], rand_choises[1], rand_choises[2])
                    update_browser_lang_field(str(rand_choises[0] + ";" + rand_choises[1] + ";" + rand_choises[2]), sandboxies_names[i])
                else:
                    rand_choises = (choises[random.randint(0,6)], choises[random.randint(7,12)])
                    data["intl"]["accept_languages"] = "{}, {}".format(rand_choises[0], rand_choises[1])
                    update_browser_lang_field(str(rand_choises[0] + ";" + rand_choises[1]), sandboxies_names[i])
            os.remove(sb_chrome_path)
            with open(sb_chrome_path, 'w') as f:
                json.dump(data, f)
        except:
            break



with open ('result.txt', 'rb') as fp:
    sandboxies_names = pickle.load(fp)
sbie = sandboxie.Sandboxie()
run_sandboxed_chrome(sbie, sandboxies_names)